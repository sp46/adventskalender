const rlLib = require("readline")

const rl = rlLib.createInterface({
    input: process.stdin,
    output: process.stdout
});

function rev(toRev){
    return toRev.split("").reverse().join("");
}

rl.question('Enter original string > ', (answer) => {
    console.log(rev(answer));
  
    rl.close();
  });